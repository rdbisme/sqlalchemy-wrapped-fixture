import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from app.models import Base, User


@pytest.fixture(scope="session")
def engine():
    return create_engine("postgresql://postgres@db/postgres", echo=True)


@pytest.fixture(scope="session")
def tables(engine):
    Base.metadata.create_all(engine)
    yield
    Base.metadata.drop_all(engine)


@pytest.fixture
def dbsession(engine, tables, mocker):
    """Returns an sqlalchemy session, and after the test tears down everything properly."""
    connection = engine.connect()
    # begin the nested transaction
    transaction = connection.begin()
    # use the connection with the already started transaction
    session = Session(bind=connection)

    mocker.patch("app.main.Session.__new__", return_value=session)

    session.add(User(id=1))
    session.commit()
    breakpoint()

    yield session

    session.close()
    # roll back the broader transaction
    transaction.rollback()
    # put back the connection to the connection pool
    connection.close()
