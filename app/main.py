import click
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

engine = create_engine("postgresql://postgres@db/postgres", echo=True)

@click.command
@click.pass_context
def main(ctx: click.Context):
    ctx.obj = session = ctx.with_resource(Session(engine))
    assert len(session.execute("SELECT * from user_account").all()) > 0



if __name__ == "__main__":
    main()
